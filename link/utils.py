#coding=utf-8
import calendar
import hashlib
import time
from datetime import datetime
from django.utils.timezone import now
import requests
from uuid import uuid4
from base64 import b64encode
from django.db import connection,connections
from django.db import transaction
from django.core.mail import EmailMultiAlternatives
from django.template.loader import render_to_string
from link.settings import *
from django.core import serializers
from django.core.cache import cache

def convert_md5(value):
	m = hashlib.md5()
	m.update(value)
	return m.hexdigest()

def create_shorhlink():
	from shortlink.models import *
	while 1:
		unique_id = 'PA' + str(uuid4())[:6]
		try:
			ManagerLink.objects.get(shortlink = unique_id)
		except:
			return unique_id

def authenticate(username,password):
	from shortlink.models import User
	try:
		user = User.objects.get(email = username)
	except:
		user = None
	if user is None:
		return None
	if user.check_password(password):
		return user
	return None
	#except:
	#	return None

def generate_token(username):
	m = hashlib.md5()
	m.update(str(username))
	m1 = hashlib.md5()
	m1.update(str(time.time()))
	return str(uuid4().int) + m.hexdigest() + m1.hexdigest()


def validateEmail( email ):
	from django.core.validators import validate_email
	from django.core.exceptions import ValidationError
	try:
		validate_email( email )
		return True
	except ValidationError:
		return False

def checkAccountExist(username):
	from shortlink.models import User
	try:
		user = User.objects.get(email=username)
		return user
	except User.DoesNotExist:
		return None

def getAccessTokenHeader(request):
	auth_header = request.META.get('HTTP_AUTHORIZATION', None)
	if auth_header is not None:
		tokens = auth_header.split(' ')
		if len(tokens) == 2 and tokens[0] == 'Bearer':
			token = tokens[1]
			return token
	return ''

def getAccountAccessToken(request):
	from shortlink.models import *
	token = getAccessTokenHeader(request)
	if token == '':
		token = request.GET.get('access_token', '')
	if token:
		try:
			user = User.objects.get(access_token = token)
			return user
		except User.DoesNotExist:
			return None
	return None