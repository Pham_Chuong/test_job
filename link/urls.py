from django.conf.urls import patterns, include, url
from django.contrib import admin
from shortlink.views import *
import  settings
admin.autodiscover()

urlpatterns = patterns('',
	url(r'^create_shortlink$', create_shortlink, name='create_shortlink'),
	url(r'^get_link/(?P<link>.*)$', get_link),

)

if settings.DEBUG:
    import debug_toolbar
    urlpatterns += patterns('',
        url(r'^__debug__/', include(debug_toolbar.urls)),
    )