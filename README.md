=============================
[# **Link** #]
=============================

### Prerequisites ###
-------------

You will need to have the following packages installed:

	$ pip install -r requirements.txt


### Usage ###
-----
	- Start service:

	$python manage.py runserver 0.0.0.0:8000

	- Kill service:

	$ps -aux | grep python 

	$kill -9 id_process



### Roadmap ###
-------
