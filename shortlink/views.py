mport json
# from __builtin__ import enumerate
import csv
from django.db.models import Q
import xlwt
from math import ceil
from dateutil.relativedelta import relativedelta
from django.db import transaction
from django.views.decorators.csrf import csrf_exempt,ensure_csrf_cookie
from django.middleware.csrf import _get_new_csrf_key
from pytz import timezone as pytz_timezone
from django.views.decorators.http import require_http_methods
from django.http import HttpResponse
from datetime import timedelta
from django.core import serializers
from boxme.settings import *
from _mysql import connect
from shortlink.models import *
from django.db.models import Q
from django.http import HttpResponseRedirect

@require_http_methods(['POST'])
@ensure_csrf_cookie
def create_shortlink(request):
	json_data = json.loads(request.body)
	link = json_data['Link']
	if link == '' or link is None:
		data = json.dumps({'validation_messages': {'Existed': 'Link is Existed'}})
		return HttpResponse(data, content_type='application/json', status=422)

	link_ = ManagerLink()
	link_.link = link
	link_.shortlink = create_shorhlink()
	link_.status = 0
	link_.save()

	obj = {
		"Link" : link,
		"ShortLink" : link_.shortlink,
		"Time": link_.create
	}
	data = json.dumps(obj)
	return HttpResponse(data, content_type='application/json', status=200)

@require_http_methods(['GET'])
@csrf_exempt
def get_link(request, link):
	try:
		link = ManagerLink..objects.get(shortlink = link)
	except:
		link = None
	if link is None:
		data = json.dumps({'validation_messages': {'Existed': 'Link is Existed'}})
		return HttpResponse(data, content_type='application/json', status=422)
	obj = {
		"Link" : link.link,
		"ShortLink" : link.shortlink,
		"Time": link_.create
	}
	data = json.dumps(obj)
	return HttpResponseRedirect(link.link)
