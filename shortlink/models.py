from django.db import models

class ManagerLink(models.Model):
	user_id 	= models.IntegerField(blank=True, null=True)
	link_web 	= models.CharField(max_length=255, blank=True, null=True)
	shortlink 	= models.CharField(max_length=255, blank=True, null=True)
	status 		= models.IntegerField(blank=True, null=True)
	create 		= models.DateTimeField(auto_now_add=True)

	class Meta:
		app_label = 'link'
		db_table  = 'manager_link'

class User(models.Model):
	fullname 		= models.CharField(max_length=255, blank=True, null=True)
	description 	= models.TextField(default='')
	email 			= models.CharField(max_length=100, blank=True, null=True)
	phone 			= models.CharField(max_length=30, blank=True, null=True)
	status 			= models.IntegerField(default=1)
	avatar 			= models.CharField(max_length=255, blank=True, null=True)
	password 		= models.CharField(max_length=32, blank=True, null=True)
	access_token 	= models.CharField(max_length=255, blank=True, null=True)
	refresh_token 	= models.CharField(max_length=255, blank=True, null=True)
	expires_in 		= models.IntegerField(default=5183940)
	time_create 	= models.IntegerField(blank=True, null=True)
	time_last_login = models.IntegerField(blank=True, null=True)

	class Meta:
		app_label = 'link'
		db_table  = 'users'